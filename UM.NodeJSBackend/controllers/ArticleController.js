const articlesModel = require("../models/Article");

//Public method
//Create new article
const createArticle = async article => {
  let newArticle = {
    title: article.title,
    articleContent: article.articleContent,
    author: article.author
  };
  let data = await articlesModel.create(newArticle);
  return data;
};

//Get all and Search article
const getArticle = async (condition, projection, selection) => {
  let listArticle = await articlesModel
    .find(condition, projection)
    .populate("author", selection)
    .exec();
  return listArticle;
};

//Update article
const updateArticle = async (id, body, returnNew) => {
  let newArticle = await articlesModel.findByIdAndUpdate(id, body, returnNew);
  return newArticle;
};

//Delete article
const deleteArticle = async id => {
  let article = await articlesModel.findByIdAndRemove(id);
  return article;
};

const getSumary = async (month, year) => {
  let dayInMonth = new Date(year, month, 0).getDate();
  let fromDate = new Date(year, month - 1, 1);
  let toDate = new Date(year, month - 1, dayInMonth, 23, 59, 59);

  let summary = {};
  summary.total = await articlesModel.find({ createdAt: { $gte: fromDate, $lte: toDate } }).count();
  summary.published = await articlesModel.find({ createdAt: { $gte: fromDate, $lte: toDate }, publish: true }).count();
  summary.unpublish = summary.total - summary.published;

  return { result: 'success', summary: summary };
};

module.exports = {
  createArticle,
  getArticle,
  getSumary,
  updateArticle,
  deleteArticle
};

const jwt = require("jsonwebtoken");
const secretKey = require("../config").secretKey;

//Public method
const createJWT = async userId => {
  let token = await jwt.sign({ id: userId }, secretKey, {
    algorithm: "HS512",
    expiresIn: "30 days"
  });
  return token;
};
//Verify token
const verifyJWT = async token => {
  let decoded = await jwt.verify(token, key);
  return decoded;
};

module.exports = {
  createJWT,
  verifyJWT
};

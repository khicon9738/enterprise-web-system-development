const filesModel = require("../models/FileInfo");
const formidable = require("formidable");
const fileSystem = require("fs");

//Public method
const uploadImage = async data => {
  let image = await promiseUploadImg(data);
  return image;
};

const getImageList = async (filter) => {
  let imageList = await filesModel.find(filter, { _id: 0, filePath: 0 , createdAt: 0, updatedAt: 0});
  return imageList;
};
/*Lazy load - from bottom to top exclude new record.
page=0;
result=20;
page=1;
createDate=
*/
//Private method
const promiseUploadImg = data => {
  return new Promise((resolve, reject) => {
    let form = formidable.IncomingForm({
      uploadDir: "./upload/image",
      keepExtensions: true,
      maxFieldsSize: 10 * 1024 * 1024,
      multiples: false
    });
    form.parse(data, async (err, fields, files) => {
      if (err) {
        reject({
          result: "failed",
          message: "Cannot upload image. Error: " + err.message
        });
      } else {
        let file = files.uploadedFile;
        if (file && file !== "null" && file !== "undefined") {
          //Replace \\ with / to make both server and window can run.
          let fileName = file.path.replace(/\\/g, "/");
          fileName = fileName.split("/")[2];
          let newFile = {
            title: fileName,
            filePath: "./upload/image/" + fileName
          };
          await filesModel.create(newFile);
          resolve({ result: "success", message: "Upload File Successfully." });
        } else {
          reject({
            result: "failed",
            message: "No file to upload!"
          });
        }
      }
    });
  });
};

module.exports = {
  uploadImage,
  getImageList
};

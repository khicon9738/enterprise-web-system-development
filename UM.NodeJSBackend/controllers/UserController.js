const userModel = require("../models/User");
const bcryptjs = require("bcryptjs");
const config = require("../config");

//Create user
const createUser = async data => {
  let hashedPassword = await hashPassword(data.password);
  let newUser = {
    username: data.username,
    password: hashedPassword,
    firstName: data.firstName,
    lastName: data.lastName,
    email: data.email,
    role: data.role
  };
  let user = await userModel.create(newUser);
  return user;
};

//Login
const loginUser = async loginInfo => {
  let loginusername = loginInfo.username;
  let user = await userModel.findOne({ username: loginusername });

  if (!user) {
    throw { result: "Login fail", message: "Username doesn't exist." };
  } else {
    let isPasswordValid = await bcryptjs.compare(
      loginInfo.password.concat(config.pwd),
      user.password
    );
    if (!isPasswordValid) {
      throw { result: "Login failed", message: "Wrong password." };
    } else {
      return {
        result: "Login success",
        user: {
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          role: user.role
        }
      };
    }
  }
};

//Get user
const getUser = async (condittion, projection) => {
  let listEmail = await userModel.find(condittion, projection);
  return listEmail;
};

//Private method
const hashPassword = async password => {
  let salt = await bcryptjs.genSalt(config.salt);
  let hashedPassword = await bcryptjs.hash(password.concat(config.pwd), salt);
  return hashedPassword;
};

module.exports = {
  createUser,
  loginUser,
  getUser
};

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const UserSchema = new Schema(
  {
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, require: true, unique: true },
    role: { type: ObjectId, ref: 'Role', required: true },
    faculty: { type: ObjectId, ref: 'Faculty', required: true }
  },
  { timestamps: true }
);

mongoose.model("User", UserSchema);
module.exports = mongoose.model("User");

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const FileSchema = new Schema(
    {
        title: { type: String, required: true },
        filePath: { type: String, required: true },
        fileTye:{type: String, required: true},
        uploader: {type: ObjectId, ref: 'User'} //Enable later when has User function.
    },
    { timestamps: true }
);

mongoose.model('FileInfo', FileSchema);
module.exports = mongoose.model('FileInfo');
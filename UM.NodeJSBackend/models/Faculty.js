const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FacultySchema = new Schema(
    {
        faculty: { type: String, required: true },
        description: { type: String }
    },
    { timestamps: true }
);

mongoose.model("Faculty", FacultySchema);
module.exports = mongoose.model("Faculty");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const ArticleSchema = new Schema(
  {
    title: { type: String, require: true },
    articleContent: { type: String, require: true },
    publish: { type: Boolean, default: false, require: true },
    author: {type: ObjectId, ref: 'User'} //Enable later when has User function.
  },
  { timestamps: true }
);

mongoose.model("Article", ArticleSchema);
module.exports = mongoose.model("Article");

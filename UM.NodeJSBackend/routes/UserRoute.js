const express = require("express");
const router = express.Router();
const userController = require("../controllers/UserController");

//Register
router.post("/register", async (req, res) => {
  try {
    let user = await userController.createUser(req.body);
    res.status(200).send({
      result: "success",
      message: "User " + user.username + " created"
    });
  } catch (err) {
    res.status(500).send({ result: "failed", message: err.message });
  }
});

//login
router.post("/login", async (req, res) => {
  try {
    let result = await userController.loginUser(req.body);
    res.status(202).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get coordinator email
router.get("/coordinator-email", async (req, res) => {
  try {
    let emails = await userController.getUser({ role: 2 }, { _id: 0, email: 1 });
    res.status(200).send({ result: "success", listEmail: emails });
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router;

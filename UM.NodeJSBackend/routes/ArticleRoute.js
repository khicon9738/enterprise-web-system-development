const express = require("express");
const router = express.Router();
const articleController = require("../controllers/ArticleController");

//Create new article
router.post("/", async (req, res) => {
  try {
    let article = await articleController.createArticle(req.body);
    res.status(200).send(article);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get all articles
router.get("/get-all-articles", async (req, res) => {
  try {
    let listArticle = await articleController.getArticle({}, {}, {});
    res.status(200).send(listArticle);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get all published articles
router.get("/published", async (req, res) => {
  try {
    let listArticle = await articleController.getArticle(
      { publish: true },
      { updatedAt: 0 },
      { _id: 0, firstName: 1, lastName: 1 }
    );
    res.status(200).send(listArticle);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get article by student
router.get("", async (req, res) => {
  try {
    let listArticle = await articleController.getArticle(
      { author: req.query.studentId },
      //{ updatedAt: 0, createdAt: 0, author: 0, __v: 0 },
      {},
      {}
    );
    res.status(200).send(listArticle);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get articles summary
router.get("/summary", async (req, res) => {
  try {
    let result = await articleController.getSumary(req.query.m, req.query.y);
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get one article
router.get("/:articleId", async (req, res) => {
  try {
    let article = await articleController.getArticle(
      {
        _id: req.params.articleId
      },
      {},
      {}
    );
    res.status(200).send(article);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Update article
router.put("/update/:articleId", async (req, res) => {
  try {
    let newArticle = await articleController.updateArticle(
      req.params.articleId,
      req.body,
      { new: true }
    );
    res.status(200).send(newArticle);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Delete article
router.delete("/:articleId", async (req, res) => {
  try {
    let article = await articleController.deleteArticle(req.params.articleId);
    res.status(200).send(article);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Test route
router.get("/ping", (req, res) => {
  res.send("Ok");
});

module.exports = router;

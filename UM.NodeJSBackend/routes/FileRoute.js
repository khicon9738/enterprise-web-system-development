const express = require("express");
const router = express.Router();
const fileController = require("../controllers/FileController");

//Upload image
router.post("/upload_image", async (req, res) => {
  try {
    let result = await fileController.uploadImage(req);
    res.send(result);
  } catch (err) {
    res.send(err);
  }
});

//Get all image
router.get("/images", async (req, res) => {
  try {
    let imageList = await fileController.getImageList({});
    res.status(200).send(imageList);
  } catch (err) {
    res.send({ result: "failed", message: err.message });
  }
});

router.get("/ping", (req, res) => {
  res.send("ok");
});

module.exports = router;

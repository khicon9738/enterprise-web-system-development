const express = require("express");
const app = express();
const db = require("./db");
const path = require("path");
const bodyParser = require("body-parser");

//Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/image", express.static(path.join(__dirname, "upload/image/")));

//Router
const UserRoute = require("./routes/UserRoute");
app.use('/api/users', UserRoute);
const FileRoute = require("./routes/FileRoute");
app.use("/api/files", FileRoute);
const ArticleRoute = require("./routes/ArticleRoute");
app.use("/api/articles", ArticleRoute);

module.exports = app;

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UM.WebApp.Common;
using UM.WebApp.Models;

namespace UM.WebApp.Controllers
{
    public class HomeController : BaseController
    {
        private HttpClient _client = null;

        public HomeController()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Constants.BASE_API_URL);
        }

        #region public methods

        // GET: Home
        public async Task<ActionResult> Index()
        {
            ArticleSummaryModel articleSummaryModel = new ArticleSummaryModel();
            var curMonth = DateTime.Now.Month.ToString();
            var curYear = DateTime.Now.Year.ToString();

            var response = await _client.GetAsync(String.Format(Constants.GET_ARTICLE_SUMMARY_API, curMonth, curYear)).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                articleSummaryModel = JsonConvert.DeserializeObject<ArticleSummaryModel>(responseContent);
                articleSummaryModel.Month = Enum.GetName(typeof(Constants.Months), int.Parse(curMonth));
                articleSummaryModel.Year = curYear;
            }

            return View(articleSummaryModel);
        }

        #endregion

        #region private methods



        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using UM.WebApp.Common;

namespace UM.WebApp.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var loginSession = Session[Constants.LOGIN_SESSION_KEY];
            //var loginSession = (UserLogin)Session[Constants.LOGIN_SESSION_KEY];

            if (loginSession == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary( new { controller = "Login", action = "Index" } ));
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
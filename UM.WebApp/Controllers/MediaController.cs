﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UM.WebApp.Common;
using UM.WebApp.Models;

namespace UM.WebApp.Controllers
{
    public class MediaController : BaseController
    {
        private HttpClient _client = null;

        public MediaController()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Constants.BASE_API_URL);
        }

        // GET: Media
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(HttpPostedFileBase mediaToUpload)
        {
            if(Global.IsRightFileExt(mediaToUpload, Constants.ARR_IMG_FILE_EXT))
            {
                var content = new MultipartFormDataContent();
                content.Add(Global.ConvertFileToByteArray(mediaToUpload));
                var response = await _client.PostAsync(Constants.IMAGE_UPLOAD_API, content).ConfigureAwait(false);
                var responseString = await response.Content.ReadAsStringAsync();

                var responseMsg = JsonConvert.DeserializeObject<ResponseModel>(responseString);

                if (response.IsSuccessStatusCode)
                {
                    ViewData[Constants.SUCCESS_RESPONSE_MSG] = responseMsg.Message;
                    return View();
                }

                ViewData[Constants.FAILED_RESPONSE_MSG] = responseMsg.Message;
                return View();
            }

            ViewData[Constants.FAILED_RESPONSE_MSG] = "Please choose a .jpg or .jpeg file to upload!";
            return View();
        }

        public ActionResult Library()
        {
            return View();
        }
    }
}
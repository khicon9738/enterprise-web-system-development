﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Threading.Tasks;
using UM.WebApp.Common;
using UM.WebApp.Models;
using Newtonsoft.Json;
using System.Net.Mail;
using Rotativa;
using System.IO;
using System.IO.Compression;
using Rotativa.Options;

namespace UM.WebApp.Controllers
{
    public class ArticleController : BaseController
    {
        private HttpClient _client = null;

        #region public method
        public ArticleController()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Constants.BASE_API_URL);
        }

        // GET: Article
        public async Task<ActionResult> Index()
        {
            IEnumerable<ArticleModel> articleList = null;

            var currentLogin = (LoginSessionModel)Session[Constants.LOGIN_SESSION_KEY];
            var loginRole = currentLogin.Role;

            switch (loginRole)
            {
                case (int)Constants.Roles.Coordinator:
                    articleList = await GetAllArticle();
                    break;

                case (int)Constants.Roles.Manager:
                    articleList = await GetAllArticle();
                    break;

                case (int)Constants.Roles.Student:
                    articleList = await GetListArticleByStudent(currentLogin.Id);
                    break;
            }

            return View(articleList.OrderByDescending(a => a.CreatedDate));
        }

        [HttpGet]
        public async Task<ActionResult> EditArticle(string _Id)
        {
            ArticleModel article = null;
            var response = await _client.GetAsync(string.Format(Constants.GET_ARTICLE_DETAIL_API + "{0}", _Id)).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                List<ArticleModel> listArticle = JsonConvert.DeserializeObject<List<ArticleModel>>(responseContent);
                article = listArticle.First();
                GetCreatedDateOnly(article);
                return View(article);
            }
            return View("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditArticle(ArticleModel article, string submit)
        {
            ChangePublishStatusModel publishArticleModel = new ChangePublishStatusModel();

            switch (submit)
            {
                case "Publish":
                    if (await ChangePublishStatus(article, publishArticleModel))
                    {
                        ViewData[Constants.SUCCESS_RESPONSE_MSG] = "Update article successfully";
                        return RedirectToAction("EditArticle", "Article", new { article._Id });
                    }
                    break;
                case "Unpublish":
                    if (await ChangePublishStatus(article, publishArticleModel))
                    {
                        ViewData[Constants.SUCCESS_RESPONSE_MSG] = "Update article successfully";
                        return RedirectToAction("EditArticle", "Article", new { article._Id });
                    }
                    break;
            }
            ViewData[Constants.FAILED_RESPONSE_MSG] = "Updated article failed! Please try again";
            return View("EditArticle", new { article._Id });
        }

        //==================This won't be used anymore=================
        public async Task<JsonResult> ArticleDetail(string _Id)
        {
            ArticleModel article = null;
            var response = await _client.GetAsync(string.Format(Constants.GET_ARTICLE_DETAIL_API + "{0}", _Id)).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                List<ArticleModel> listArticle = JsonConvert.DeserializeObject<List<ArticleModel>>(responseContent);
                article = listArticle.First();
                GetCreatedDateOnly(article);

                return Json(article, JsonRequestBehavior.AllowGet);
            }

            return Json("Error getting article detail");
        }
        //============================================================

        [HttpGet]
        public ActionResult AddNewArticle()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddNewArticle(ArticleModel article)
        {
            var currentStudent = (LoginSessionModel)Session[Constants.LOGIN_SESSION_KEY];

            ArticleForAddNewModel newArticle = new ArticleForAddNewModel()
            {
                title = article.Title,
                articleContent = article.ArticleContent,
                author = currentStudent.Id
            };

            var response = await _client.PostAsJsonAsync<ArticleForAddNewModel>(Constants.ADD_NEW_ARTICLE_API, newArticle).ConfigureAwait(false);
            var responseString = await response.Content.ReadAsStringAsync();
            var responseMsg = JsonConvert.DeserializeObject<ResponseAddArticleModel>(responseString);

            if (response.IsSuccessStatusCode)
            {
                var newArticleId = responseMsg._Id;
                //send notification email
                await SendNotificationEmail(newArticleId);
            }

            return RedirectToAction("Index", "Article");
        }

        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> ViewDetail(string _Id)
        {
            var article = await GetArticleDetail(_Id);
            return View(article);
        }

        public async Task<ActionResult> ExportToPDF(string _Id)
        {
            var article = await GetArticleDetail(_Id);
            var articlePdf = new PartialViewAsPdf("~/Views/Shared/ArticlePDF.cshtml", article);
            return articlePdf;
        }

        public async Task<ActionResult> ExportAllToPDF()
        {
            var articleList = await GetAllArticle();

            var archive = Server.MapPath("~/App_Data/articles.zip");
            var temp = Server.MapPath("~/Temp/");

            if (System.IO.File.Exists(archive))
            {
                System.IO.File.Delete(archive);
            }

            foreach (var article in articleList)
            {
                var articleToPdf = await GetArticleDetail(article._Id);
                var pdfFile = new PartialViewAsPdf("~/Views/Shared/ArticlePDF.cshtml", articleToPdf)
                {
                    FileName = article.Title + ".pdf",
                    PageSize = Size.A4
                };
                byte[] pdfData = pdfFile.BuildFile(ControllerContext);
                System.IO.File.WriteAllBytes(temp + pdfFile.FileName, pdfData);
            }

            ZipFile.CreateFromDirectory(temp, archive);
            //delete article pdf files in Temp folder after zip
            Directory.EnumerateFiles(temp).ToList().ForEach(f => System.IO.File.Delete(f));

            return File(archive, "application/zip", "articles.zip");
        }

        #endregion

        #region private method

        /// <summary>
        /// Get all article
        /// </summary>
        /// <returns>Article list</returns>
        private async Task<IEnumerable<ArticleModel>> GetAllArticle()
        {
            IEnumerable<ArticleModel> articleList = null;

            var response = await _client.GetAsync(Constants.GET_ARTICLE_LIST_API).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                articleList = JsonConvert.DeserializeObject<List<ArticleModel>>(responseContent);
                foreach (var article in articleList)
                {
                    GetCreatedDateOnly(article);
                }
            }

            return articleList;
        }

        /// <summary>
        /// Get articles based on login student
        /// </summary>
        /// <param name="strStdId">Login student id</param>
        /// <returns>Article list</returns>
        private async Task<IEnumerable<ArticleModel>> GetListArticleByStudent(string strStdId)
        {
            IEnumerable<ArticleModel> articleList = null;

            var response = await _client.GetAsync(Constants.GET_ARTICLE_LIST_BY_STUDENT_API + strStdId).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                articleList = JsonConvert.DeserializeObject<List<ArticleModel>>(responseContent);
                foreach (var article in articleList)
                {
                    GetCreatedDateOnly(article);
                }
            }

            return articleList;
        }

        /// <summary>
        /// Substring CreatedAt to get the date only
        /// </summary>
        /// <param name="articleList">Article list</param>
        /// <returns>created date</returns>
        private void GetCreatedDateOnly(ArticleModel article)
        {
            var createdAt = article.CreatedAt;
            var createdDate = createdAt.Substring(0, 10);
            article.CreatedDate = createdDate;
        }

        /// <summary>
        /// Change publish status of an article
        /// </summary>
        /// <param name="article">old status article</param>
        /// <param name="publishArticleModel">new status article</param>
        /// <returns>true if change status successfully</returns>
        private async Task<bool> ChangePublishStatus(ArticleModel article, ChangePublishStatusModel publishArticleModel)
        {
            article.Publish = !article.Publish;
            publishArticleModel = new ChangePublishStatusModel
            {
                publish = article.Publish
            };

            var response = await _client.PutAsJsonAsync<ChangePublishStatusModel>(string.Format(Constants.UPDATE_ARTICLE_API + "{0}", article._Id), publishArticleModel).ConfigureAwait(false);
            //var responseString = await response.Content.ReadAsStringAsync();
            //var responseMsg = JsonConvert.DeserializeObject<ArticleModel>(responseString);
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Send notification email when a new article is uploaded
        /// </summary>
        private async Task SendNotificationEmail(string newArticleId)
        {
            SmtpClient smtpClient = new SmtpClient();
            var emailSubject = "[University Magazine]New Uploaded Article";
            var emailBody = "<p>A student has uploaded a new article.</p>";
            emailBody += "<p>Link to the new uploaded article: " + GetNewArticleDetailUrl(newArticleId) + "</p>";
            emailBody += "<p>You have 14 days to add comment for this new contribution.</p>";

            //list coordinator email to send to
            var lstCoorEmail = await Global.GetAllCoordinatorEmail();
            foreach (var receiver in lstCoorEmail)
            {
                Global.SendGmail(receiver, emailSubject, emailBody);
            }
        }

        /// <summary>
        /// Get the url for new added article detail
        /// </summary>
        /// <param name="newArticleId">New article id</param>
        /// <returns>New article detail url</returns>
        private string GetNewArticleDetailUrl(string newArticleId)
        {
            var request = Request;
            var baseUrl = string.Format("{0}://{1}", request.Url.Scheme, request.Url.Authority);
            var newArticleDetailUrl = baseUrl + "/Article/ViewDetail?_Id=" + newArticleId;
            return newArticleDetailUrl;
        }

        private async Task<ArticleModel> GetArticleDetail(string articleId)
        {
            ArticleModel article = new ArticleModel();
            var response = await _client.GetAsync(string.Format(Constants.GET_ARTICLE_DETAIL_API + "{0}", articleId)).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                List<ArticleModel> listArticle = JsonConvert.DeserializeObject<List<ArticleModel>>(responseContent);
                article = listArticle.First();
                GetCreatedDateOnly(article);
            }

            return article;
        }

        #endregion
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UM.WebApp.Common;
using UM.WebApp.Models;

namespace UM.WebApp.Controllers
{
    public class LoginController : Controller
    {
        private HttpClient _client = null;

        public LoginController()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Constants.BASE_API_URL);
        }

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Login(LoginModel login)
        {
            if (ModelState.IsValid)
            {
                LoginSessionModel sessionModel = null;
                var response = await _client.PostAsJsonAsync<LoginModel>(Constants.LOGIN_API, login).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    var responseUser = JsonConvert.DeserializeObject<LoginResponseModel>(responseString);
                    sessionModel = responseUser.User;

                    Session.Add(Constants.LOGIN_SESSION_KEY, sessionModel);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Incorrect username or password!");
                }
            }

            return View("Index");
        }

        public ActionResult Logout()
        {
            var loginSession = Session[Constants.LOGIN_SESSION_KEY];
            if(loginSession != null)
            {
                Session.Remove(Constants.LOGIN_SESSION_KEY);
                return RedirectToAction("Index", "Login");
            }

            return View("Index");
        }
    }
}
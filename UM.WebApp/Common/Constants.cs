﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UM.WebApp.Common
{
    public static class Constants
    {
        #region Api Url
        public const string BASE_API_URL = "http://54.169.251.218:3000/api/";
        //public const string BASE_API_URL = "http://localhost:3000/api/";
        public const string IMAGE_UPLOAD_API = "files/upload_image";

        //article api
        public const string GET_ARTICLE_LIST_API = "articles/get-all-articles";
        public const string GET_ARTICLE_DETAIL_API = "articles/";
        public const string ADD_NEW_ARTICLE_API = "articles";
        public const string UPDATE_ARTICLE_API = "articles/update/";
        public const string GET_ALL_PUBLISH_ARTICLE_API = "articles/published";
        public const string GET_ARTICLE_LIST_BY_STUDENT_API = "articles?studentId=";
        public const string GET_ARTICLE_SUMMARY_API = "articles/summary?m={0}&y={1}";

        //user api
        public const string LOGIN_API = "users/login";
        public const string REGISTER_API = "users/register";
        public const string GET_ALL_COORDINATOR_EMAIL_API = "users/coordinator-email";
        #endregion

        #region File Extension Array
        public static readonly string[] ARR_IMG_FILE_EXT = { ".jpg", ".jpeg" };
        public static readonly string[] ARR_WORD_FILE_EXT = { ".doc", ".docx" };
        #endregion

        #region ViewData String
        public const string SUCCESS_RESPONSE_MSG = "successResponseMsg";
        public const string FAILED_RESPONSE_MSG = "failedResponseMsg";
        #endregion

        #region Session string
        public const string LOGIN_SESSION_KEY = "loginSession";

        public enum Roles
        {
            Administrator = 0,
            Manager = 1,
            Coordinator = 2,
            Student = 3,
            Guest = 4
        };
        #endregion

        public enum Months
        {
            January = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12
        }
    }
}
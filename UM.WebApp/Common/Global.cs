﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using UM.WebApp.Models;

namespace UM.WebApp.Common
{
    public static class Global
    {
        /// <summary>
        /// Check extension of a file
        /// </summary>
        /// <param name="file">A file</param>
        /// <param name="arrFileExt">Array of extension to check</param>
        /// <returns>True if match value in array</returns>
        public static bool IsRightFileExt(HttpPostedFileBase file, string[] arrFileExt)
        {
            if (file != null)
            {
                var fileName = Path.GetFileName(file.FileName);
                var fileExt = Path.GetExtension(fileName);

                for (int i = 0; i < arrFileExt.Length; i++)
                {
                    if (fileExt.Equals(arrFileExt[i])) return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Convert file to array of byte
        /// </summary>
        /// <param name="file">A file</param>
        /// <returns>Array of byte</returns>
        public static ByteArrayContent ConvertFileToByteArray(HttpPostedFileBase file)
        {
            byte[] bytes = new byte[file.InputStream.Length + 1];
            file.InputStream.Read(bytes, 0, bytes.Length);
            var fileContent = new ByteArrayContent(bytes);
            fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data")
            {
                Name = "uploadedFile",
                FileName = file.FileName
            };

            return fileContent;
        }

        /// <summary>
        /// Send gmail
        /// </summary>
        /// <param name="receiverEmail">receiver email</param>
        /// <param name="emailSubject">subject of email</param>
        /// <param name="emailBody">body of email</param>
        public static void SendGmail(string receiverEmail, string emailSubject, string emailBody)
        {
            SmtpClient smtpClient = new SmtpClient();
            var message = new MailMessage();
            message.To.Add(receiverEmail);
            message.Subject = emailSubject;
            message.Body = emailBody;
            message.IsBodyHtml = true;
            smtpClient.Send(message);
        }

        /// <summary>
        /// Get all coordinator email
        /// </summary>
        /// <returns>List of coordinator email</returns>
        public static async Task<List<string>> GetAllCoordinatorEmail()
        {
            List<string> lstCoorEmail = new List<string>();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(Constants.BASE_API_URL);

            var response = await client.GetAsync(Constants.GET_ALL_COORDINATOR_EMAIL_API).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                var result = JsonConvert.DeserializeObject<CoordinatorEmailResponseModel>(responseContent);
                foreach (var email in result.ListEmail)
                {
                    if (!email.Email.Equals("tungnt123@gmail.com")) //remove later after delete this coordinator
                    {
                        lstCoorEmail.Add(email.Email);
                    }
                }
            }

            return lstCoorEmail;
        }


    }
}
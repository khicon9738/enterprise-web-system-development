﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UM.WebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "CustomGetDetail",
                url: "Article/GetArticleDetail/{_Id}",
                defaults: new { controller = "Article", action = "GetArticleDetail", _Id = UrlParameter.Optional },
                namespaces: new string[] { "UM.WebApp.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "UM.WebApp.Controllers" }
            );
        }
    }
}

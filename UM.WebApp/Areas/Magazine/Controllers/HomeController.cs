﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UM.WebApp.Areas.Magazine.Models;
using UM.WebApp.Common;

namespace UM.WebApp.Areas.Magazine.Controllers
{
    public class HomeController : Controller
    {
        private HttpClient _client = null;

        #region public method
        public HomeController()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Constants.BASE_API_URL);
        }

        // GET: Magazine/Home
        public async Task<ActionResult> Index()
        {
            IEnumerable<ArticleModel> publishedArticleList = null;

            var response = await _client.GetAsync(Constants.GET_ALL_PUBLISH_ARTICLE_API).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                publishedArticleList = JsonConvert.DeserializeObject<List<ArticleModel>>(responseContent);
                foreach (var article in publishedArticleList)
                {
                    GetCreatedDateOnly(article);
                }
            }

            return View(publishedArticleList.OrderByDescending(a => a.CreatedDate));
        }

        #endregion

        #region private method

        /// <summary>
        /// Substring CreatedAt to get the date only
        /// </summary>
        /// <param name="articleList">Article list</param>
        /// <returns>created date</returns>
        private void GetCreatedDateOnly(ArticleModel article)
        {
            var createdAt = article.CreatedAt;
            var createdDate = createdAt.Substring(0, 10);
            article.CreatedDate = createdDate;
        }

        #endregion
    }
}
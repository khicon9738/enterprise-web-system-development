﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UM.WebApp.Areas.Magazine.Models;
using UM.WebApp.Common;

namespace UM.WebApp.Areas.Magazine.Controllers
{
    public class ArticleController : Controller
    {
        private HttpClient _client = null;

        #region public method
        public ArticleController()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Constants.BASE_API_URL);
        }

        public async Task<ActionResult> Detail(string _Id)
        {
            ArticleModel article = null;
            var response = await _client.GetAsync(string.Format(Constants.GET_ARTICLE_DETAIL_API + "{0}", _Id)).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                List<ArticleModel> listArticle = JsonConvert.DeserializeObject<List<ArticleModel>>(responseContent);
                article = listArticle.First();
                GetCreatedDateOnly(article);

                return View(article);
            }

            return View("Error");
        }

        #endregion

        #region private method

        /// <summary>
        /// Substring CreatedAt to get the date only
        /// </summary>
        /// <param name="articleList">Article list</param>
        /// <returns>created date</returns>
        private void GetCreatedDateOnly(ArticleModel article)
        {
            var createdAt = article.CreatedAt;
            var createdDate = createdAt.Substring(0, 10);
            article.CreatedDate = createdDate;
        }

        #endregion
    }
}
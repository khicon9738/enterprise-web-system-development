﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UM.WebApp.Areas.Magazine.Models
{
    public class ArticleModel
    {
        public string _Id { get; set; }

        public string Title { get; set; }

        [AllowHtml]
        public string ArticleContent { get; set; }

        public AuthorModelInArticle Author { get; set; }

        public string CreatedAt { get; set; }

        public string CreatedDate { get; set; }
    }

    public class AuthorModelInArticle
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
﻿function getDetailArticle(_Id) {
    var articleTitle = $("#articleTitle");
    var articleContent = $("#articleContent");

    $.ajax({
        url: "/Article/GetArticleDetail/" + _Id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            articleTitle.html(result.Title);
            articleContent.html(result.ArticleContent);
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UM.WebApp.Models
{
    public class ArticleSummaryModel
    {
        public string Result { get; set; }

        public SummaryModel Summary { get; set; }

        public string Month { get; set; }

        public string Year { get; set; }
    }

    public class SummaryModel
    {
        public int Total { get; set; }

        public int Published { get; set; }

        public int Unpublish { get; set; }
    }
}
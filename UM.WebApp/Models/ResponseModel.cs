﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UM.WebApp.Models
{
    public class ResponseModel
    {
        public string Result { get; set; }

        public string Message { get; set; }
    }
}
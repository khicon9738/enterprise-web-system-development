﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UM.WebApp.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Username is required!")]
        public string username { get; set; }

        [Required(ErrorMessage = "Password is required!")]
        public string password { get; set; }
    }


    public class LoginSessionModel
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Role { get; set; }
    }

    public class LoginResponseModel
    {
        public string Result { get; set; }

        public LoginSessionModel User { get; set; }
    }
}
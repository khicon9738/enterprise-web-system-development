﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UM.WebApp.Models
{
    public class FileModel
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string FilePath { get; set; }

        public string FileType { get; set; }

        public string Uploader { get; set; }
    }
}
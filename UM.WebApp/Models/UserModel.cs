﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UM.WebApp.Models
{
    public class UserModel
    {
        public string _Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }
    }

    public class CoordinatorEmailResponseModel
    {
        public string Result { get; set; }

        public List<UserEmailModel> ListEmail { get; set; }
    }

    public class UserEmailModel
    {
        public string Email { get; set; }
    }
}
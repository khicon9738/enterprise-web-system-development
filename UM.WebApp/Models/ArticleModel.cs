﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UM.WebApp.Models
{
    public class ArticleModel
    {
        public string _Id { get; set; }

        public string Title { get; set; }

        [AllowHtml]
        public string ArticleContent { get; set; }

        public bool Publish { get; set; }

        public AuthorModelInArticle Author { get; set; }

        public string CreatedAt { get; set; }

        public string CreatedDate { get; set; }
    }

    public class ArticleForAddNewModel
    {
        public string title { get; set; }

        [AllowHtml]
        public string articleContent { get; set; }

        public bool publish { get; set; }

        public string author { get; set; }
    }

    public class AuthorModelInArticle
    {
        public string _Id { get; set; }

        public string Username { get; set; }
    }

    public class ArticleByStudentModel
    {
        public string _Id { get; set; }

        public string Title { get; set; }

        [AllowHtml]
        public string ArticleContent { get; set; }

        public bool Publish { get; set; }
    }

    public class ChangePublishStatusModel
    {
        public bool publish { get; set; }
    }

    public class ResponseAddArticleModel
    {
        public string _Id { get; set; }
    }
}